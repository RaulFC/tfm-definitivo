import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint
import numba

from numba.core.errors import NumbaDeprecationWarning, NumbaPendingDeprecationWarning
import warnings
warnings.simplefilter('ignore', category=NumbaDeprecationWarning)
warnings.simplefilter('ignore', category=NumbaPendingDeprecationWarning)

#np.seterr(divide='ignore', invalid='ignore')

"""FUNCIONES"""

def set_axes_equal(ax):
    '''Make axes of 3D plot have equal scale so that spheres appear as spheres,
    cubes as cubes, etc..  This is one possible solution to Matplotlib's
    ax.set_aspect('equal') and ax.axis('equal') not working for 3D.

    Input
      ax: a matplotlib axis, e.g., as output from  plt.gca().
    '''

    x_limits = ax.get_xlim3d()
    y_limits = ax.get_ylim3d()
    z_limits = ax.get_zlim3d()

    x_range = abs(x_limits[1] - x_limits[0])
    x_middle = np.mean(x_limits)
    y_range = abs(y_limits[1] - y_limits[0])
    y_middle = np.mean(y_limits)
    z_range = abs(z_limits[1] - z_limits[0])
    z_middle = np.mean(z_limits)

    # The plot bounding box is a sphere in the sense of the infinity
    # norm, hence I call half the max range the plot radius.
    plot_radius = 0.5*max([x_range, y_range, z_range])

    ax.set_xlim3d([x_middle - plot_radius, x_middle + plot_radius])
    ax.set_ylim3d([y_middle - plot_radius, y_middle + plot_radius])
    ax.set_zlim3d([z_middle - plot_radius, z_middle + plot_radius])


def dib_cab(a,b,c,xr,xs,yr,ys,zr,ZS,ax):

    ax.plot3D([0, a, a, 0, 0],[0, 0, b, b, 0],[0, 0, 0, 0, 0],'k')
    ax.plot3D([0, a, a, 0, 0],[0, 0, b, b, 0],[c, c, c, c, c],'k')
    ax.plot3D([0, a, a, 0, 0],[0, 0, 0, 0, 0],[0, 0, c, c, 0],'k')
    ax.plot3D([0, a, a, 0, 0],[b, b, b, b, b],[0, 0, c, c, 0],'k')
    ax.scatter3D(Xe,Ye,ZS,color = 'blue')
    ax.scatter3D(xr,yr,zr, color ='red')
    ax.plot3D([xs, xs],[ys, ys],[0, ZS],':k')
    ax.plot3D([xr, xr],[yr, yr],[0, zr],':k')
    ax.set_xlim(-0.1*a, a + a*0.1)
    ax.set_ylim(-0.1*b, b + b*0.1)
    ax.set_zlim(-0.1*c, c + c*0.1)
    ax.set_xlabel("x(m)")
    ax.set_ylabel("y(m)")
    ax.set_zlabel("z(m)")

def dib_cab2(a,b,c,xr,xs,yr,ys,zr,ZS,ax):

    ax.plot3D([0, a, a, 0, 0],[0, 0, b, b, 0],[0, 0, 0, 0, 0],'k')
    ax.plot3D([0, a, a, 0, 0],[0, 0, b, b, 0],[c, c, c, c, c],'k')
    ax.plot3D([0, a, a, 0, 0],[0, 0, 0, 0, 0],[0, 0, c, c, 0],'k')
    ax.plot3D([0, a, a, 0, 0],[b, b, b, b, b],[0, 0, c, c, 0],'k')
    ax.scatter3D(Xe,Ye,ZS,color = 'blue')
    ax.scatter3D(Xe2,b,Ze2,color = 'blue')
    ax.scatter3D(a,Ye3,Ze3,color = 'blue')
    #ax.scatter3D(xr,yr,zr, color ='red')
    ax.plot3D([xs, xs],[ys, ys],[0, ZS],':k')
    ax.plot3D([xr, xr],[yr, yr],[0, zr],':k')
    ax.set_xlim(-0.1*a, a + a*0.1)
    ax.set_ylim(-0.1*b, b + b*0.1)
    ax.set_zlim(-0.1*c, c + c*0.1)
    ax.set_xlabel("x(m)")
    ax.set_ylabel("y(m)")
    ax.set_zlabel("z(m)")


def frec_nat():
    klmn = []
    l = []
    m = []
    n = []
    N = int(np.ceil(max([a, b, c])*fmax/c0))
    lenI = int(np.floor(a/max([a, b, c])))
    lenJ = int(np.floor(b/max([a, b, c])))
    lenK = int(np.floor(c/max([a, b, c])))
    for i in range(lenI,N):
        for j in range(lenJ,N):
            for k in range(lenK,N):
                klmn.append(np.sqrt((i*np.pi/a)**2 + (j*np.pi/b)**2 + (k*np.pi/c)**2))
                l.append(i)
                m.append(j)
                n.append(k)
    wlmn = [abs(c0*q) for q in klmn]
    return l,m,n,wlmn


def resp_esp(l,m,n,wlmn,x,xq,y,yq,z,zq):
    w = 2*np.pi*f0
    k = w/c0
    p = 0
    for i in range(len(wlmn)):
        wN = wlmn[i]
        kN = wN/c0
        for em in range(len(xq)):
            AN = 8*1j*w*rho0*Q*np.cos(l[i]*np.pi*xq[em]/a)*np.cos(m[i]*np.pi*yq[em]/b)*np.cos(n[i]*np.pi*zq/c)/(a*b*c*(kN**2 - k**2 + (4*1j*k/znp)*(1/a + 1/b + 1/c)))
            p += AN*np.cos(l[i]*np.pi*x/a)*np.cos(m[i]*np.pi*y/b)*np.cos(n[i]*np.pi*z/c)
    return p

def resp_frec(l,m,n,wlmn,x,xq,y,yq,z,zq):
    p = 0
    for i in range(len(wlmn)):
        wN = wlmn[i]
        kN = wN/c0
        for em in range(len(xq)):
            AN = 8*1j*w*rho0*Q*np.cos(l[i]*np.pi*xq[em]/a)*np.cos(m[i]*np.pi*yq[em]/b)*np.cos(n[i]*np.pi*zq/c)/(a*b*c*(kN**2 - K**2 + (4*1j*K/znp)*(1/a + 1/b + 1/c)))
            p += AN*np.cos(l[i]*np.pi*x/a)*np.cos(m[i]*np.pi*y/b)*np.cos(n[i]*np.pi*z/c)
    return p

def Radio(y0, t):
    R, u = y0
    return u, (P_g-P_0-Pmax*np.sin(ws*t)-2*sigma/R-
4*mu*u/R+((2*sigma/R_0+P_0-P_g)*(R_0/R)**(3*ks))*(1-
3*ks*u/cs))/(R*rho)-3*u**2/(2*R)


"""PARÁMETROS"""

a = 0.1                    #coordenada x (m)
b = 0.2                    #coordenada y (m)
c = 0.3                    #coordenada z (m)
c0 = 1500                  #velocidad del sonido en el medio (agua) en m/s
rho0 = 1000                #densidad del medio (agua) en kg/m^3
f0 = 25000                 #frecuencia que se emite desde la fuente (Hz)
fmax = f0+3000             #frecuencia máxima examinada (Hz)
fmin = f0-3000             #frecuencia mínima examinada (Hz)

znp = float("inf")

f = np.arange(fmin,fmax,5) #rango de frecuencias
w = 2*np.pi*f              #rango de frecuencias angulares
K = w/c0                   #números de onda
xr = a/2                   #coordenada x receptor
yr = b/2                   #coordenada y receptor
zr = c/2                   #coordenada z receptor

L,M,N,WLMN = frec_nat()

#Puntos emisores del pistón

R_Piston = 0.02            #Radio del pistón (m)
desp = 5.4e-7
Q =desp*2*np.pi*f0*np.pi*R_Piston**2      #volume source strength

rp = np.linspace(0,R_Piston,100)
theta = np.linspace(0,2*np.pi,200)
Xe = []
Ye = []
for i in theta:
    for j in rp:
        Xe.append(j*np.cos(i)+a/2)
        Ye.append(j*np.sin(i)+b/2)

Xe2 = []
Ze2 = []
for i in theta:
    for j in rp:
        Xe2.append(j*np.cos(i)+a/2)
        Ze2.append(j*np.sin(i)+c/2)

Ye3 = []
Ze3 = []
for i in theta:
    for j in rp:
        Ye3.append(j*np.cos(i)+b/2)
        Ze3.append(j*np.sin(i)+c/2)

xs = a/2                   #coordenada x emisor
ys = b/2                   #coordenada y emisor
Ze = [c]*len(Xe)                     #coordenada z emisor
ZS = c
XS = [a]*len(Xe)
YS = [b]*len(Xe)

@numba.njit(parallel=True, fastmath=True)
def getLPOnePoint(WLMN, mAN, resta, L, M, N, Xe, Ye, Ze, cx, cy, cz ):
    tsuma = np.zeros(len(w),dtype=np.complex_)
    for i in numba.prange(len(WLMN)):
        dAN = abc*((WLMN[i]/c0)**2 - resta)
        for em in range(len(Xe)):
            AN = mAN*np.cos(L[i]*np.pi*Xe[em]/a)*np.cos(M[i]*np.pi*Ye[em]/b)*np.cos(N[i]*np.pi*Ze[em]/c)/dAN
            tsuma += AN*np.cos(L[i]*np.pi*cx/a)*np.cos(M[i]*np.pi*cy/b)*np.cos(N[i]*np.pi*cz/c)
    return tsuma

@numba.njit(parallel=True, fastmath=True)
def getAllPreasures(WLMN, mAN, resta, L, M, N, Xe, Ye, Ze, leng):
    tsuma = np.zeros((leng,leng),dtype=np.complex_)
    for i in numba.prange(len(WLMN)):
        dAN = abc*((WLMN[i]/c0)**2 - resta)
        for em in range(len(Xe)):
            AN = mAN*np.cos(L[i]*np.pi*Xe[em]/a)*np.cos(M[i]*np.pi*Ye[em]/b)*np.cos(N[i]*np.pi*Ze[em]/c)/dAN
            tsuma += AN*np.cos(L[i]*np.pi*X/a)*np.cos(M[i]*np.pi*Y/b)*np.cos(N[i]*np.pi*Z/c)
        print(i)
    return tsuma

@numba.njit(parallel=True, fastmath=True)
def maxPreasure(mAN, dAN, L, M, N, Xe, Ye, Ze ):
    lenX = len(X)
    lenY = len(Y)
    lenZ = len(Z)
    suma = np.empty((len(Xe),lenX*lenY*lenZ),dtype=np.complex_)
    for em in numba.prange(len(Xe)):
        AN = mAN*np.cos(L*np.pi*Xe[em]/a)*np.cos(M*np.pi*Ye[em]/b)*np.cos(N*np.pi*Ze[em]/c)/dAN
        #AN1 = mAN*np.cos(L*np.pi*Xe2[em]/a)*np.cos(M*np.pi*YS[em]/b)*np.cos(N*np.pi*Ze2[em]/c)/dAN
        #AN2 = mAN*np.cos(L*np.pi*XS[em]/a)*np.cos(M*np.pi*Ye3[em]/b)*np.cos(N*np.pi*Ze3[em]/c)/dAN
        #ANtot = [AN[j] + AN1[j] + AN2[j] for j in range(len(AN))]
        for cx in range(lenX):
            for cy in range(lenY):
                for cz in range(lenZ):
                    suma[em][cx*lenY*lenZ+cy*lenZ+cz] = AN*np.cos(L*np.pi*X[cx]/a)*np.cos(M*np.pi*Y[cy]/b)*np.cos(N*np.pi*Z[cz]/c)
    return np.abs(suma.sum(axis=0))


"""
MENU:
1: modos y frecuencias
2: respuesta en frecuencia en un punto
3h: respuesta espacial total plano horizontal(perpendicular al eje z)
3vy: respuesta espacial total plano vertical (perpendicular al eje y)
3vx: respuesta espacial total plano vertical (perpendicular al eje x)
4: Devuelve la presión máxima en todo el recipiente
"""

if __name__ == '__main__':
    caso = 4
    caso2 = 2
    caso3 = "h"
    abc = a*b*c
    d_abc1 = 1/a + 1/b + 1/c
    m_8j_rho = 8*1j*rho0*Q
    lp = []
    pres = []
    X = np.linspace(0,a,40)
    Y = np.linspace(0,b,40)
    Z = np.linspace(0,c,40)
    total = 0
    if caso == 1:
        Ms = []
        Mat = [np.conj(np.transpose(WLMN))/(2*np.pi), np.conj(np.transpose(L)), np.conj(np.transpose(M)), np.conj(np.transpose(N))]
        for i in Mat:
            Ms.append(sorted(i))
        plt.plot(Ms[:][0],'ob')
        plt.xlabel('n')
        plt.ylabel('$f_{mnl}$ (Hz)')
        plt.show()

    elif caso == 2:
        if caso2 ==1: #Lp 3 puntos diferentes para un transductor
            cx = a/2
            cy = b/2
            cz = c/2
            mAN = m_8j_rho*w
            resta = K**2 + (4*1j*K/znp)*d_abc1
            p = getLPOnePoint(WLMN, mAN, resta, L, M, N, Xe, Ye, Ze, cx, cy, cz)
            p2 = getLPOnePoint(WLMN, mAN, resta, L, M, N, Xe, Ye, Ze, a/4, b/4, c/4)
            p3 = getLPOnePoint(WLMN, mAN, resta, L, M, N, Xe, Ye, Ze, a/4, b/3, c/5)
            Lp1 = 20*np.log10(np.abs(p)/1e-6)
            Lp2 = 20*np.log10(np.abs(p2)/1e-6)
            Lp3 = 20*np.log10(np.abs(p3)/1e-6)
            fig = plt.figure()
            ax = plt.axes(projection='3d')
            dib_cab(a,b,c,xr,xs,yr,ys,zr,ZS,ax)
            plt.figure()
            plt.plot(f,Lp1/max(*Lp1,*Lp2,*Lp3), label = "Punto central (a/2, b/2, c/2)")
            plt.plot(f,Lp2/max(*Lp1,*Lp2,*Lp3), label = "Punto (a/4, b/4, c/4)")
            plt.plot(f,Lp3/max(*Lp1,*Lp2,*Lp3), label = "Punto (a/4, b/3, c/5)")
            plt.legend()
            plt.xlim(fmin, fmax)
            plt.xlabel('Hz')
            plt.ylabel('Lp (dB) normalizado')
            set_axes_equal(ax)
            plt.show()
        if caso2 ==2: #Lp punto central para tres transductores
            cx = a/2
            cy = b/2
            cz = c/2
            mAN = m_8j_rho*w
            resta = K**2 + (4*1j*K/znp)*d_abc1
            p = getLPOnePoint(WLMN, mAN, resta, L, M, N, Xe, Ye, Ze, 0.9*a, b/2, c/2)
            p2 = getLPOnePoint(WLMN, mAN, resta, L, M, N, Xe2, YS, Ze2, 0.9*a, b/2, c/2)
            p3 = getLPOnePoint(WLMN, mAN, resta, L, M, N, XS, Ye3, Ze3, 0.9*a, b/2, c/2)
            pa = getLPOnePoint(WLMN, mAN, resta, L, M, N, Xe, Ye, Ze, a/2, 0.9*b, c/2)
            pa2 = getLPOnePoint(WLMN, mAN, resta, L, M, N, Xe2, YS, Ze2, a/2, 0.9*b, c/2)
            pa3 = getLPOnePoint(WLMN, mAN, resta, L, M, N, XS, Ye3, Ze3, a/2, 0.9*b, c/2)
            pb = getLPOnePoint(WLMN, mAN, resta, L, M, N, Xe, Ye, Ze, a/2, b/2, 0.9*c)
            pb2 = getLPOnePoint(WLMN, mAN, resta, L, M, N, Xe2, YS, Ze2, a/2, b/2, 0.9*c)
            pb3 = getLPOnePoint(WLMN, mAN, resta, L, M, N, XS, Ye3, Ze3, a/2, b/2, 0.9*c)
            pc = getLPOnePoint(WLMN, mAN, resta, L, M, N, Xe, Ye, Ze, cx,cy,cz)
            pc2 = getLPOnePoint(WLMN, mAN, resta, L, M, N, Xe2, YS, Ze2, cx,cy,cz)
            pc3 = getLPOnePoint(WLMN, mAN, resta, L, M, N, XS, Ye3, Ze3, cx,cy,cz)
            ptot = [p[i] + p2[i] + p3[i] for i in range(len(p))]
            patot = [pa[i] + pa2[i] + pa3[i] for i in range(len(pa))]
            pbtot = [pb[i] + pb2[i] + pb3[i] for i in range(len(pb))]
            pctot = [pc[i] + pc2[i] + pc3[i] for i in range(len(pc))]
            Lp = 20*np.log10(np.abs(ptot)/1e-6)
            Lpa = 20*np.log10(np.abs(patot)/1e-6)
            Lpb = 20*np.log10(np.abs(pbtot)/1e-6)
            Lpc = 20*np.log10(np.abs(pctot)/1e-6)
            fig = plt.figure()
            ax = plt.axes(projection='3d')
            dib_cab2(a,b,c,xr,xs,yr,ys,zr,ZS,ax)
            plt.figure()
            plt.plot(f,Lpc/max(*Lp,*Lpa,*Lpb,*Lpc), label = "Punto central (a/2, b/2, c/2)")
            plt.plot(f,Lp/max(*Lp,*Lpa,*Lpb,*Lpc), label = "Punto (0.9*a, b/2, c/2)")
            plt.plot(f,Lpa/max(*Lp,*Lpa,*Lpb,*Lpc), label = "Punto (a/2, 0.9*b, c/2)")
            plt.plot(f,Lpb/max(*Lp,*Lpa,*Lpb,*Lpc), label = "Punto (a/2, b/2, 0.9*c)")
    
            plt.legend()
            plt.xlim(fmin, fmax)
            plt.xlabel('Frecuencia(Hz)')
            plt.ylabel('Lp (dB) normalizado')
            set_axes_equal(ax)
            plt.show()

    elif caso == 3:
        leng = len(X)
        if caso3 == "h":
            X,Y = np.meshgrid(X,Y)
            Z = c/2
        if caso3 == "vy":
            X,Z = np.meshgrid(X,Z)
            Y = b/2
        if caso3 == "vx":
            leng = len(Y)
            Y,Z = np.meshgrid(Y,Z)
            X = a/2
        w = 2*np.pi*f0
        K = w/c0
        mAN = m_8j_rho*w
        resta = K**2 + (4*1j*K/znp)*d_abc1
        total1 = getAllPreasures(WLMN, mAN, resta, L, M, N, Xe, Ye, Ze, leng)
        total2 = getAllPreasures(WLMN, mAN, resta, L, M, N, Xe2, YS, Ze2, leng)
        total3 = getAllPreasures(WLMN, mAN, resta, L, M, N, XS, Ye3, Ze3, leng)
        tot = [total1[i] + total2[i] + total3[i] for i in range(len(total1))]
        fig = plt.figure()
        ax = plt.axes(projection='3d')
        if caso3 == "h":
            color = ax.contourf(X,Y,np.abs(tot),zdir="z",offset=zr)
        if caso3 == "vy":
            color = ax.contourf(X,np.abs(tot),Z,zdir="y",offset=yr)
        if caso3 == "vx":
            color = ax.contourf(np.abs(tot),Y,Z,zdir="x",offset=xr)
        fig.colorbar(color)
        dib_cab2(a,b,c,xr,xs,yr,ys,zr,ZS,ax)
        set_axes_equal(ax)
        plt.show()

    elif caso == 4:
        X = np.linspace(0,a,10)
        Y = np.linspace(0,b,10)
        Z = np.linspace(0,c,10)
        Pmax = 0
        ptot = 0
        for i in range(len(WLMN)):
            resta = K[i]**2 + (4*1j*K[i]/znp)*d_abc1
            mAN = m_8j_rho*w[i]
            kN = WLMN[i]/c0
            dAN = abc*(kN**2 - resta+0.0001)
            total41 = maxPreasure(mAN, dAN, L[i], M[i], N[i], Xe, Ye, Ze)
            total42 = maxPreasure(mAN, dAN, L[i], M[i], N[i], Xe2, YS, Ze2)
            total43 = maxPreasure(mAN, dAN, L[i], M[i], N[i], XS, Ye3, Ze3)
            tot4 = total41 + total42 + total43
            total4 = max(tot4)
            print(i)
            if ptot<total4:
                ptot = total4
        Pmax = ptot
        print(Pmax)

    # Condiciones iniciales

    R_0 = 5e-6
    u_0 = 0

    # Parámetros

    rho = 1000                  #densidad del agua (kg/m^3) (792 para metanol)
    sigma = 0.0725              #tensión superficial (N/m) (0.0226 para metanol)
    mu = 8.9*10**(-4)           #viscosidad del agua (Pa*s) (0.56e-3 para metanol)
    P_0 = 101325+rho*9.8*c*0.5  #presión a la que está sometida la burbuja (Pa)
    P_g = P_0                   #presión del gas dentro de la burbuja (Pa)

    ks = 5/3                    #índice politrópico
    P_A = 135000                #amplitud onda incidente (Pa)
    fs = 25000                  #frecuencia onda incidente (Hz)
    ws = 2*np.pi*fs             #frecuencia angular onda incidente (rad/s)
    cs = 1500                   #velocidad sonido agua (m/s) (1143 para metanol)

    # Definimos el tiempo de simulación

    tiempo = np.arange(0, 0.000130, 0.000000000025)

    # Resolvemos las ecuaciones

    R_2 = odeint(Radio, [R_0, u_0], tiempo)

    V2 = R_2[:,1]
    Rex = R_2[:,0]*10**6
    tiemporeesc = tiempo*10**6

    print("Radio máximo = ", max(Rex), "micrómetros")
    print("Radio mínimo = ", min(Rex), "micrómetros")

    # Representación gráfica

    plt.plot(tiemporeesc, Rex)
    plt.xlabel("tiempo ($\mu$s)")
    plt.ylabel("Radio ($\mu$m)")
    plt.show()

    plt.plot(tiemporeesc, V2)
    plt.xlabel("tiempo ($\mu$s)")
    plt.ylabel("Velocidad (m/s)")
    plt.show()


    fig, ax1 = plt.subplots()  
  
    color = 'tab:blue'
    ax1.set_xlabel("tiempo ($\mu$s)")  
    ax1.set_ylabel("Radio ($\mu$m)", color = color)  
    ax1.plot(tiemporeesc, Rex, color = color)  
    ax1.tick_params(axis ='y', labelcolor = color)  
  
    ax2 = ax1.twinx()  
  
    color = 'tab:orange'
    ax2.set_ylabel("Velocidad (m/s)", color = color)  
    ax2.plot(tiemporeesc, V2, color = color)  
    ax2.tick_params(axis ='y', labelcolor = color)  
    
    plt.show()

    # Presión dentro de la burbuja

    Rexterno = R_2[:,0]
    P = (2*sigma/R_0+P_0)*(R_0/Rexterno)**(3*ks)

    plt.plot(tiemporeesc, P)
    plt.xlabel("tiempo ($\mu$s)")
    plt.ylabel("Presión (Pa)")
    plt.show()

    # Temperatura dentro de la burbuja

    T_0 = 300 # Temperatura inicial (K)
    T = T_0 * (R_0/Rexterno)**(3*(ks-1))

    plt.plot(tiemporeesc, T)
    plt.xlabel("tiempo ($\mu$s)")
    plt.ylabel("Temperatura (K)")
    plt.show()
