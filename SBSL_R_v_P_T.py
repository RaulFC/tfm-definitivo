import numpy as np
from matplotlib import pyplot as plt
from scipy.integrate import odeint

# Definición de funciones

def Radio(y0, t):
    R, u = y0
    return u, (P_g-P_0-P_A*np.sin(w*t)-2*sigma/R- 
4*mu*u/R+((2*sigma/R_0+P_0-P_g)*(R_0/R)**(3*k))*(1-
3*k*u/c))/(R*rho)-3*u**2/(2*R)

# Condiciones iniciales

R_0 = 5e-6
u_0 = 0

# Parámetros 

rho = 1000            #densidad del agua (kg/m^3) (792 para metanol)
sigma = 0.0725        #tensión superficial (N/m) (0.0226 para metanol)
mu = 8.9*10**(-4)     #viscosidad del agua (Pa*s) (0.56e-3 para metanol)
P_0 = 101325          #presión a la que está sometida la burbuja (Pa)
P_g = 2000            #presión del gas dentro de la burbuja (Pa)

k = 5/3               #índice politrópico
P_A = 135000          #amplitud onda incidente (Pa)
f = 25000             #frecuencia onda incidete (Hz)
w = 2*np.pi*f         #frecuencia angular onda incidente (rad/s)
c = 1500              #velocidad sonido agua (m/s) (1143 para metanol)

# Definimos el tiempo de simulación

tiempo = np.arange(0, 0.000165, 0.000000000025)

# Resolvemos las ecuaciones

R_2 = odeint(Radio, [R_0, u_0], tiempo)

V2 = R_2[:,1]
Rex = R_2[:,0]*10**6
tiemporeesc = tiempo*10**6

print("Radio máximo = ", max(Rex), "micrómetros")
print("Radio mínimo = ", min(Rex), "micrómetros")

# Representación gráfica

plt.plot(tiemporeesc, Rex)
plt.xlabel("tiempo ($\mu$s)")
plt.ylabel("Radio ($\mu$m)")
plt.show()

plt.plot(tiemporeesc, V2)
plt.xlabel("tiempo ($\mu$s)")
plt.ylabel("Velocidad (m/s)")
plt.show()


fig, ax1 = plt.subplots()  
  
color = 'tab:blue'
ax1.set_xlabel("tiempo ($\mu$s)")  
ax1.set_ylabel("Radio ($\mu$m)", color = color)  
ax1.plot(tiemporeesc, Rex, color = color)  
ax1.tick_params(axis ='y', labelcolor = color)  
  
ax2 = ax1.twinx()  
  
color = 'tab:orange'
ax2.set_ylabel("Velocidad (m/s)", color = color)  
ax2.plot(tiemporeesc, V2, color = color)  
ax2.tick_params(axis ='y', labelcolor = color)  
 
plt.show()

# Presión dentro de la burbuja

Rexterno = R_2[:,0]
P = (2*sigma/R_0+P_0)*(R_0/Rexterno)**(3*k)

plt.plot(tiemporeesc, P)
plt.xlabel("tiempo ($\mu$s)")
plt.ylabel("Presión (Pa)")
plt.show()

# Temperatura dentro de la burbuja

T_0 = 300 # Temperatura inicial (K)
T = T_0 * (R_0/Rexterno)**(3*(k-1))

plt.plot(tiemporeesc, T)
plt.xlabel("tiempo ($\mu$s)")
plt.ylabel("Temperatura (K)")
plt.show()
