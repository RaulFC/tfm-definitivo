import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator
from mpl_toolkits.mplot3d import axes3d
from scipy.special import jn
from scipy.integrate import odeint

#np.seterr(divide='ignore', invalid='ignore')

"""FUNCIONES"""

def set_axes_equal(ax):
    '''Make axes of 3D plot have equal scale so that spheres appear as spheres,
    cubes as cubes, etc..  This is one possible solution to Matplotlib's
    ax.set_aspect('equal') and ax.axis('equal') not working for 3D.

    Input
      ax: a matplotlib axis, e.g., as output from plt.gca().
    '''

    x_limits = ax.get_xlim3d()
    y_limits = ax.get_ylim3d()
    z_limits = ax.get_zlim3d()

    x_range = abs(x_limits[1] - x_limits[0])
    x_middle = np.mean(x_limits)
    y_range = abs(y_limits[1] - y_limits[0])
    y_middle = np.mean(y_limits)
    z_range = abs(z_limits[1] - z_limits[0])
    z_middle = np.mean(z_limits)

    # The plot bounding box is a sphere in the sense of the infinity
    # norm, hence I call half the max range the plot radius.
    plot_radius = 0.5*max([x_range, y_range, z_range])

    ax.set_xlim3d([x_middle - plot_radius, x_middle + plot_radius])
    ax.set_ylim3d([y_middle - plot_radius, y_middle + plot_radius])
    ax.set_zlim3d([z_middle - plot_radius, z_middle + plot_radius])


def dib_cab(a,b,c,xr,xs,yr,ys,zr,zs,ax):
   
    ax.plot3D([0, a, a, 0, 0],[0, 0, b, b, 0],[0, 0, 0, 0, 0],'k')
    ax.plot3D([0, a, a, 0, 0],[0, 0, b, b, 0],[c, c, c, c, c],'k')
    ax.plot3D([0, a, a, 0, 0],[0, 0, 0, 0, 0],[0, 0, c, c, 0],'k')
    ax.plot3D([0, a, a, 0, 0],[b, b, b, b, b],[0, 0, c, c, 0],'k')
    ax.scatter3D(Xe,Ye,zs,color = 'blue')
    ax.scatter3D(xr,yr,zr, color ='red')
    ax.plot3D([xs, xs],[ys, ys],[0, zs],':k')
    ax.plot3D([xr, xr],[yr, yr],[0, zr],':k')
    ax.set_xlim(-0.1*a, a + a*0.1)
    ax.set_ylim(-0.1*b, b + b*0.1)
    ax.set_zlim(-0.1*c, c + c*0.1)
    

def frec_nat():
    klmn = []
    #wlmn = []
    l = []
    m = []
    n = []
    N = int(np.ceil(max([a, b, c])*fmax/c0))
    for i in range(int(np.floor(a/max([a, b, c]))),N):
        for j in range(int(np.floor(b/max([a, b, c]))),N):
            for k in range(int(np.floor(c/max([a, b, c]))),N):
                klmn.append(np.sqrt((i*np.pi/a)**2 + (j*np.pi/b)**2 + (k*np.pi/c)**2))
                l.append(i)
                m.append(j)
                n.append(k)
    wlmn = [np.sqrt((c0**2)*(q**2)) for q in klmn]
    """for q in klmn:
        wlmn.append(np.sqrt((c0**2)*(q**2)))"""
    return l,m,n,wlmn


def resp_esp(l,m,n,wlmn,x,xq,y,yq,z,zq):
    w = 2*np.pi*f0
    k = w/c0
    p = 0
    for i in range(1,len(wlmn)):
        wN = wlmn[i]
        kN = wN/c0
        for em in range(len(xq)):
            AN = 8*1j*w*rho0*Q*np.cos(l[i]*np.pi*xq[em]/a)*np.cos(m[i]*np.pi*yq[em]/b)*np.cos(n[i]*np.pi*zq/c)/(a*b*c*(kN**2 - k**2 + (4*1j*k/znp)*(1/a + 1/b + 1/c)))
            p += AN*np.cos(l[i]*np.pi*x/a)*np.cos(m[i]*np.pi*y/b)*np.cos(n[i]*np.pi*z/c)
    return p


def resp_frec(l,m,n,wlmn,x,xq,y,yq,z,zq):
    p = 0
    for i in range(1,len(wlmn)):
        wN = wlmn[i]
        kN = wN/c0
        for em in range(len(xq)):
            AN = 8*1j*w*rho0*Q*np.cos(l[i]*np.pi*xq[em]/a)*np.cos(m[i]*np.pi*yq[em]/b)*np.cos(n[i]*np.pi*zq/c)/(a*b*c*(kN**2 - k**2 + (4*1j*k/znp)*(1/a + 1/b + 1/c)))
            p += AN*np.cos(l[i]*np.pi*x/a)*np.cos(m[i]*np.pi*y/b)*np.cos(n[i]*np.pi*z/c)
    return p

"""PARÁMETROS"""

a = 0.1                    #coordenada x (m)
b = 0.2                    #coordenada y (m)
c = 0.3                    #coordenada z (m)
c0 = 1500                  #velocidad del sonido en el medio (agua) en m/s
rho0 = 1000                #densidad del medio (agua) en kg/m^3
f0 = 22500                 #frecuencia que se emite desde la fuente (Hz)
fmax = 25000               #frecuencia máxima examinada (Hz)
fmin = 20000               #frecuencia mínima examinada (Hz)

znp = float("inf")

Q =0.01                    #volume source strength

f = np.arange(fmin,fmax,5) #rango de frecuencias
w = 2*np.pi*f              #rango de frecuencias angulares
k = w/c0                   #números de onda
xr = a/2                   #coordenada x receptor
yr = b/2                   #coordenada y receptor
zr = c/2                   #coordenada z receptor

l,m,n,wlmn = frec_nat()

l = frec_nat()[0]
m = frec_nat()[1]
n = frec_nat()[2]
wlmn = frec_nat()[3]

#Puntos emisores del pistón

R_Piston = 0.02            #Radio del pistón (m)

rp = np.linspace(0,R_Piston,100)
theta = np.linspace(0,2*np.pi,100)
Xe = []
Ye = []
for i in theta:
    for j in rp:
        Xe.append(j*np.cos(i)+a/2)
        Ye.append(j*np.sin(i)+b/2)

xs = a/2                   #coordenada x emisor
ys = b/2                   #coordenada y emisor
zs = c                     #coordenada z emisor


"""
MENU:
1: modos y frecuencias
2: respuesta en frecuencia en un punto
3h: respuesta espacial total plano horizontal(perpendicular al eje z)
3vy: respuesta espacial total plano vertical (perpendicular al eje y)
3vx: respuesta espacial total plano vertical (perpendicular al eje x)
"""

caso = "2"

if caso == "1":
    Ms = []
    M = [np.conj(np.transpose(wlmn))/(2*np.pi), np.conj(np.transpose(l)), np.conj(np.transpose(m)), np.conj(np.transpose(n))]
    for i in M:
        Ms.append(sorted(i))
    plt.plot(Ms[:][0],'ob')
    plt.xlabel('n')
    plt.ylabel('$f_{mnl}$ (Hz)')
    plt.show()

elif caso == "2":
    p = resp_frec(l,m,n,wlmn,xr,Xe,yr,Ye,zr,zs)
    print(max(20*np.log10(abs(p)/2e-5)))
    fig = plt.figure()
    ax = plt.axes(projection='3d')
    dib_cab(a,b,c,xr,xs,yr,ys,zr,zs,ax)
    plt.figure()
    plt.plot(f,20*np.log10(abs(p)/2e-5))
    plt.xlim(fmin, fmax)
    plt.xlabel('Hz')
    plt.ylabel('Lp (dB)')
    set_axes_equal(ax)
    plt.show()

elif caso == "2m":
    lp = []
    pres = []
    X = np.linspace(0,a,3)
    Y = np.linspace(0,b,3)
    Z = np.linspace(0,c,3)
    """for i in X: for j in Y: for k in Z: p = resp_frec(l,m,n,wlmn,i,Xe,j,Ye,k,zs)
    print(p)
    pres = [(abs(max(p)),i,j,k) for i in X for j in Y for k in Z]
    lp = [(abs(max(20*np.log10(p/2e-5))),i,j,k) for i in X for j in Y for k in Z]
    """
    for i in X:
        for j in Y:
            for k in Z:
                p = resp_frec(l,m,n,wlmn,i,Xe,j,Ye,k,zs)
                pres.append([max(abs(p)),i,j,k])
                lp.append([max(20*np.log10(abs(p)/2e-5)),i,j,k])
    Lpmax = max(lp)
    Presmax = max(pres)
    Pmax = Presmax[0]
    xpmax = Presmax[1]
    ypmax = Presmax[2]
    zpmax = Presmax[3]
    #print(Pmax)

# Definición de funciones

def Radio(y0, t):
    R, u = y0
    return u, (P_g-P_0-Pmax*np.sin(ws*t)-2*sigma/R- 
4*mu*u/R+((2*sigma/R_0+P_0-P_g)*(R_0/R)**(3*ks))*(1-
3*ks*u/cs))/(R*rho)-3*u**2/(2*R)

# Condiciones iniciales

R_0 = 5e-6
u_0 = 0

# Parámetros 

rho = 1000            #densidad del agua (kg/m^3) (792 para metanol)
sigma = 0.0725        #tensión superficial (N/m) (0.0226 para metanol)
mu = 8.9*10**(-4)     #viscosidad del agua (Pa*s) (0.56e-3 para metanol)
P_0 = 101325          #presión a la que está sometida la burbuja (Pa)
P_g = 2000            #presión del gas dentro de la burbuja (Pa)

ks = 5/3               #índice politrópico
P_A = 135000          #amplitud onda incidente (Pa)
fs = 25000             #frecuencia onda incidete (Hz)
ws = 2*np.pi*fs         #frecuencia angular onda incidente (rad/s)
cs = 1500              #velocidad sonido agua (m/s) (1143 para metanol)

# Definimos el tiempo de simulación

tiempo = np.arange(0, 0.000225, 0.000000000025)

# Resolvemos las ecuaciones

R_2 = odeint(Radio, [R_0, u_0], tiempo)

V2 = R_2[:,1]
Rex = R_2[:,0]*10**6
tiemporeesc = tiempo*10**6

print("Radio máximo = ", max(Rex), "micrómetros")
print("Radio mínimo = ", min(Rex), "micrómetros")

# Representación gráfica

plt.plot(tiemporeesc, Rex)
plt.xlabel("tiempo ($\mu$s)")
plt.ylabel("Radio ($\mu$m)")
plt.show()

# Presión dentro de la burbuja

Rexterno = R_2[:,0]
P = (2*sigma/R_0+P_0)*(R_0/Rexterno)**(3*ks)

plt.plot(tiemporeesc, P)
plt.xlabel("tiempo ($\mu$s)")
plt.ylabel("Presión (Pa)")
plt.show()

# Temperatura dentro de la burbuja

T_0 = 300 # Temperatura inicial (K)
T = T_0 * (R_0/Rexterno)**(3*(ks-1))

plt.plot(tiemporeesc, T)
plt.xlabel("tiempo ($\mu$s)")
plt.ylabel("Temperatura (K)")
plt.show()
    
    


    

